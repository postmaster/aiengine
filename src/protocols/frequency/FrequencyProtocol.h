/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_FREQUENCY_FREQUENCYPROTOCOL_H_
#define SRC_PROTOCOLS_FREQUENCY_FREQUENCYPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include <cstring>
#include "Frequencies.h"
#include "PacketFrequencies.h"
#include "Cache.h"
#include "flow/FlowManager.h"

namespace aiengine {

class FrequencyProtocol: public Protocol {
public:
    	explicit FrequencyProtocol(const std::string& name, const std::string& short_name);
	explicit FrequencyProtocol():FrequencyProtocol("FrequencyProtocol","frequency") {}
    	virtual ~FrequencyProtocol() {}
	
	static const uint16_t id = 0;
	static const int header_size = 2;
	static const int DefaultInspectionLimit = 100; // Number of packets process for compute the frequencies

	int getHeaderSize() const { return header_size;}

	bool processPacket(Packet& packet) { return true; }
	void processFlow(Flow *flow);

	void statistics(std::basic_ostream<char>& out, int level);
	void statistics(std::basic_ostream<char>& out) { statistics(out, stats_level_); }

	void releaseCache(); 

        void setHeader(unsigned char *raw_packet) {
        
                freq_header_ = raw_packet;
        }

	// All the flows are processed by the frequency proto
	bool freqChecker(Packet &packet); 

        void createFrequencies(int number); 
        void destroyFrequencies(int number); 
	
	void setCacheManager(SharedPointer<CacheManager> cmng); 
	void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	int64_t getCurrentUseMemory() const;
	int64_t getAllocatedMemory() const; 
	int64_t getTotalAllocatedMemory() const; 

        void setDynamicAllocatedMemory(bool value);
	bool isDynamicAllocatedMemory() const;

	CounterMap getCounters() const { CounterMap counters; return counters; }

private:
	unsigned char *freq_header_;
	int inspection_limit_;
	Cache<Frequencies>::CachePtr freqs_cache_;
	Cache<PacketFrequencies>::CachePtr packet_freqs_cache_;
	FlowManagerPtrWeak flow_mng_;
};

typedef std::shared_ptr<FrequencyProtocol> FrequencyProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_FREQUENCY_FREQUENCYPROTOCOL_H_
