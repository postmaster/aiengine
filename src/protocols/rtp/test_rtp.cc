/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_rtp.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE rtptest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(rtp_test_suite, StackRTPtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet;

	BOOST_CHECK(rtp->getTotalBytes() == 0);
	BOOST_CHECK(rtp->getTotalPackets() == 0);
	BOOST_CHECK(rtp->getTotalValidatedPackets() == 0);
	BOOST_CHECK(rtp->getTotalMalformedPackets() == 0);
	BOOST_CHECK(rtp->processPacket(packet) == true);
	
	CounterMap c = rtp->getCounters();
}

BOOST_AUTO_TEST_CASE (test02)
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_rtp_h223_1);
        int length = raw_packet_ethernet_ip_udp_rtp_h223_1_length;
        Packet packet(pkt, length);

	inject(packet);

        Flow *flow = rtp->getCurrentFlow();

        BOOST_CHECK( flow != nullptr);

	BOOST_CHECK( rtp->getTotalBytes() == 167);
	BOOST_CHECK( rtp->getTotalPackets() == 1);
	BOOST_CHECK( rtp->getTotalValidatedPackets() == 1);
	BOOST_CHECK( rtp->getTotalMalformedPackets() == 0);

	BOOST_CHECK( rtp->getPayloadType() == 99); //Clear mode
	BOOST_CHECK( rtp->getPadding() == false); //Clear mode

	CounterMap c = rtp->getCounters();
}

BOOST_AUTO_TEST_CASE (test03) // malformed packet
{
	unsigned char *pkt = reinterpret_cast <unsigned char*> (&(raw_packet_ethernet_ip_udp_rtp_h223_1[42]));
        int length = 10;
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->packet = const_cast<Packet*>(&packet);
        rtp->processFlow(flow.get());

        BOOST_CHECK( flow != nullptr);
	BOOST_CHECK( flow->getPacketAnomaly() == PacketAnomalyType::RTP_BOGUS_HEADER);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(ipv6_rtp_test_suite, StackIPv6RTPtest)

BOOST_AUTO_TEST_CASE (test01)
{
/*
 * TODO
 */
}

BOOST_AUTO_TEST_SUITE_END()
