/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_SIP_SIPPROTOCOL_H_
#define SRC_PROTOCOLS_SIP_SIPPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_LIBLOG4CXX
#include "log4cxx/logger.h"
#endif
#include "Protocol.h"
#include "StringCache.h"
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>
#include "CacheManager.h"
#include <unordered_map>
#include "regex/Regex.h"
#include "flow/FlowManager.h"
#include "SIPInfo.h"

namespace aiengine {

// Methods and response with statistics
typedef std::tuple<const char*, int, const char*, int32_t> SipMethodType;

class SIPProtocol: public Protocol {
public:
    	explicit SIPProtocol();
    	virtual ~SIPProtocol(); 

	static const uint16_t id = 0;
	static const int header_size = 0;

	int getHeaderSize() const { return header_size; }

	bool processPacket(Packet &packet) { /* Nothing to process at packet level*/ return true; }
	void processFlow(Flow *flow);

	void statistics(std::basic_ostream<char> &out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char> &out, int level);

	void releaseCache(); // Three caches will be clean 

        void setHeader(unsigned char *raw_packet) {
        
                sip_header_ = reinterpret_cast <unsigned char*> (raw_packet);
        }

        // Condition for say that a payload is SIP
        bool sipChecker(Packet &packet); 

	unsigned char *getPayload() { return sip_header_; }

        void increaseAllocatedMemory(int value); 
        void decreaseAllocatedMemory(int value);
        
	void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	int64_t getCurrentUseMemory() const;
	int64_t getAllocatedMemory() const;
	int64_t getTotalAllocatedMemory() const;

        void setDynamicAllocatedMemory(bool value); 
        bool isDynamicAllocatedMemory() const; 

	int32_t getTotalCacheMisses() const;
	int32_t getTotalEvents() const { return total_events_; }

	CounterMap getCounters() const; 

	void setCacheManager(SharedPointer<CacheManager> cmng) { cache_mng_ = cmng; cache_mng_->setCache(info_cache_); }
private:

	void attach_uri_to_flow(SIPInfo *info, boost::string_ref &uri);
	void attach_from_to_flow(SIPInfo *info, boost::string_ref &from);
	void attach_to_to_flow(SIPInfo *info, boost::string_ref &to);
	void attach_via_to_flow(SIPInfo *info, boost::string_ref &via);
	void extract_uri_value(SIPInfo *info, boost::string_ref &header);
	void extract_from_value(SIPInfo *info, boost::string_ref &header);
	void extract_to_value(SIPInfo *info, boost::string_ref &header);
	void extract_via_value(SIPInfo *info, boost::string_ref &header);

	int64_t compute_memory_used_by_maps() const;
	int32_t release_sip_info(SIPInfo *info);

	static std::vector<SipMethodType> methods_;
	static std::vector<SipMethodType> responses_;

	SharedPointer<Regex> sip_regex_;
	SharedPointer<Regex> sip_from_;
	SharedPointer<Regex> sip_to_;
	SharedPointer<Regex> sip_via_;
	unsigned char *sip_header_;
	int32_t total_events_;

	// Some statistics of the SIP methods 
	int32_t total_requests_;
	int32_t total_responses_;
	int32_t total_sip_others_;

	Cache<SIPInfo>::CachePtr info_cache_;
	Cache<StringCache>::CachePtr uri_cache_;
	Cache<StringCache>::CachePtr via_cache_;
	Cache<StringCache>::CachePtr from_cache_;
	Cache<StringCache>::CachePtr to_cache_;

	GenericMapType uri_map_;	
	GenericMapType via_map_;
	GenericMapType from_map_;	
	GenericMapType to_map_;	

	FlowManagerPtrWeak flow_mng_;
	SharedPointer<CacheManager> cache_mng_;
#ifdef HAVE_LIBLOG4CXX
	static log4cxx::LoggerPtr logger;
#endif
};

typedef std::shared_ptr<SIPProtocol> SIPProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_SIP_SIPPROTOCOL_H_
