/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_smtp.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE smtptest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(smtp_test_suite, StackSMTPtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet;

        BOOST_CHECK(smtp->getTotalPackets() == 0);
        BOOST_CHECK(smtp->getTotalValidatedPackets() == 0);
        BOOST_CHECK(smtp->getTotalMalformedPackets() == 0);
        BOOST_CHECK(smtp->getTotalBytes() == 0);
	BOOST_CHECK(smtp->processPacket(packet) == true);
	
	CounterMap c = smtp->getCounters();
}

BOOST_AUTO_TEST_CASE (test02)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_smtp_server_banner);
        int length = raw_packet_ethernet_ip_tcp_smtp_server_banner_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(smtp->getTotalPackets() == 1);
        BOOST_CHECK(smtp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(smtp->getTotalBytes() == 181);

        std::string cad("220-xc90.websitewelcome.com ESMTP Exim 4.69");
        std::ostringstream h;

        h << smtp->getPayload();
	
        BOOST_CHECK(cad.compare(0, cad.size(), h.str(), 0, cad.size()) == 0);

	BOOST_CHECK(smtp->getTotalEvents() == 0);
}

BOOST_AUTO_TEST_CASE (test03)
{
        char *header =  "EHLO GP\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        smtp->processFlow(flow.get());

        BOOST_CHECK(smtp->getTotalBytes() == 9);

        std::string cad("EHLO GP");
        std::ostringstream h;

        h << smtp->getPayload();

        BOOST_CHECK(cad.compare(0, cad.length(), h.str(), 0, cad.length()) == 0);
}

BOOST_AUTO_TEST_CASE (test04)
{
        char *header =  "MAIL FROM: <gurpartap@patriots.in>\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        smtp->processFlow(flow.get());

        BOOST_CHECK(smtp->getTotalBytes() == length);
	BOOST_CHECK(flow->getSMTPInfo() != nullptr);

	SharedPointer<SMTPInfo> info = flow->getSMTPInfo();
	SharedPointer<StringCache> from = info->from;
	SharedPointer<StringCache> to = info->to;

	BOOST_CHECK( info != nullptr);
	BOOST_CHECK(from != nullptr);
	BOOST_CHECK(to == nullptr);
	
        std::string cad("gurpartap@patriots.in");
        std::ostringstream h;

        h << from->getName();
        BOOST_CHECK(cad.compare(h.str()) == 0);
	BOOST_CHECK(smtp->getTotalEvents() == 0);
}

BOOST_AUTO_TEST_CASE (test05)
{
        char *header =  "RCPT TO: <mike_andersson@yahoo.me>\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt, length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        smtp->processFlow(flow.get());

        BOOST_CHECK(smtp->getTotalBytes() == length);
        BOOST_CHECK(flow->getSMTPInfo() != nullptr);

        SharedPointer<SMTPInfo> info = flow->getSMTPInfo();
        SharedPointer<StringCache> from = info->from;
        SharedPointer<StringCache> to = info->to;

        BOOST_CHECK(from == nullptr);
        BOOST_CHECK(to != nullptr);

        std::string cad("mike_andersson@yahoo.me");
        std::ostringstream h;

        h << to->getName();
        BOOST_CHECK(cad.compare(h.str()) == 0);
}

BOOST_AUTO_TEST_CASE (test06)
{
        char *header =  "MAIL FROM: <billy_the_kid@yahoo.com>\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt,length);

        auto domain_ban_mng = SharedPointer<DomainNameManager>(new DomainNameManager());
        auto domain_name = SharedPointer<DomainName>(new DomainName("unwanted domain","yahoo.com"));

        smtp->setDomainNameBanManager(domain_ban_mng);
        domain_ban_mng->addDomainName(domain_name);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        smtp->processFlow(flow.get());

        BOOST_CHECK(flow->getSMTPInfo() != nullptr);

        SharedPointer<SMTPInfo> info = flow->getSMTPInfo();
        SharedPointer<StringCache> from = info->from;
        SharedPointer<StringCache> to = info->to;

	BOOST_CHECK(domain_name->getMatchs() == 1);
        BOOST_CHECK( info != nullptr);
        BOOST_CHECK(from == nullptr);
        BOOST_CHECK(to == nullptr);
	BOOST_CHECK( info->isBanned() == true);
	BOOST_CHECK(smtp->getTotalEvents() == 0);
}

BOOST_AUTO_TEST_CASE (test07)
{
        char *header =  "MAIL FROM: <billy_the_kid@yahoo.com>\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt,length);

        auto domain_ban_mng = SharedPointer<DomainNameManager>(new DomainNameManager());
        auto domain_mng = SharedPointer<DomainNameManager>(new DomainNameManager());
        auto domain_name_ban = SharedPointer<DomainName>(new DomainName("unwanted domain","google.com"));
        auto domain_name = SharedPointer<DomainName>(new DomainName("unwanted domain","yahoo.com"));

        smtp->setDomainNameBanManager(domain_ban_mng);
        smtp->setDomainNameManager(domain_mng);
        domain_ban_mng->addDomainName(domain_name_ban);
        domain_mng->addDomainName(domain_name);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        smtp->processFlow(flow.get());

        BOOST_CHECK(flow->getSMTPInfo() != nullptr);

        SharedPointer<SMTPInfo> info = flow->getSMTPInfo();
        SharedPointer<StringCache> from = info->from;
        SharedPointer<StringCache> to = info->to;

        BOOST_CHECK(domain_name_ban->getMatchs() == 0);
        BOOST_CHECK(domain_name->getMatchs() == 1);
        BOOST_CHECK( info != nullptr);
        BOOST_CHECK(from != nullptr);
        BOOST_CHECK(to == nullptr);
        BOOST_CHECK( info->isBanned() == false);
	BOOST_CHECK(smtp->getTotalEvents() == 1);
        
	smtp->setDomainNameManager(nullptr);
}

BOOST_AUTO_TEST_CASE (test08)
{
        char *header =  "MAIL FROM: <myaexploit@yahoo.com\\x90\\x90\\x90\\x90\\x90\\x90\\x90\\x90\\x90\\x90\\x90\\x90\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt,length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        smtp->processFlow(flow.get());

	BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::SMTP_BOGUS_HEADER);
	BOOST_CHECK(smtp->getTotalEvents() == 1);
}

BOOST_AUTO_TEST_CASE (test09)
{
        char *header =  "MAIL FROM: <myuseryahoo.com>\r\n";
        unsigned char *pkt = reinterpret_cast <unsigned char*> (header);
        int length = strlen(header);
        Packet packet(pkt,length);

        auto flow = SharedPointer<Flow>(new Flow());

        flow->setFlowDirection(FlowDirection::FORWARD);
        flow->packet = const_cast<Packet*>(&packet);
        smtp->processFlow(flow.get());

	BOOST_CHECK(flow->getPacketAnomaly() == PacketAnomalyType::SMTP_BOGUS_HEADER);
	BOOST_CHECK(smtp->getTotalEvents() == 1);
}

BOOST_AUTO_TEST_CASE (test10)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_tcp_2525_smtp_server_banner);
        int length = raw_packet_ethernet_ip_tcp_2525_smtp_server_banner_length;
        Packet packet(pkt, length);

        inject(packet);

        BOOST_CHECK(smtp->getTotalPackets() == 1);
        BOOST_CHECK(smtp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(smtp->getTotalBytes() == 67);
        BOOST_CHECK(smtp->getTotalEvents() == 0);
}

BOOST_AUTO_TEST_CASE (test11)
{
        char *header1 =  "MAIL FROM: <billy_the_kid@yahoo.com>\r\n";
        unsigned char *pkt1 = reinterpret_cast <unsigned char*> (header1);
        int length1 = strlen(header1);
        Packet packet1(pkt1, length1);
        
	char *header2 =  "RCPT TO: <lovely_mayer@yahoo.co.uk>\r\n";
        unsigned char *pkt2 = reinterpret_cast <unsigned char*> (header2);
        int length2 = strlen(header2);
        Packet packet2(pkt2, length2);
        
        auto flow1 = SharedPointer<Flow>(new Flow());
        auto flow2 = SharedPointer<Flow>(new Flow());

        flow1->setFlowDirection(FlowDirection::FORWARD);
        flow1->packet = const_cast<Packet*>(&packet1);
        
	flow2->setFlowDirection(FlowDirection::FORWARD);
        flow2->packet = const_cast<Packet*>(&packet1);
       
	// Inject two times 
        smtp->processFlow(flow1.get());
        smtp->processFlow(flow2.get());

        SharedPointer<SMTPInfo> info2 = flow2->getSMTPInfo();
        SharedPointer<SMTPInfo> info1 = flow1->getSMTPInfo();

	BOOST_CHECK( info2 != nullptr);
	BOOST_CHECK( info1 != nullptr);

	BOOST_CHECK( info1->from != nullptr);
	BOOST_CHECK( info2->from != nullptr);
	BOOST_CHECK( info1->to == nullptr);
	BOOST_CHECK( info2->to == nullptr);

	std::string from("billy_the_kid@yahoo.com");
	std::string to("lovely_mayer@yahoo.co.uk");

	BOOST_CHECK( from.compare(info2->from->getName()) == 0);

	flow1->packet = const_cast<Packet*>(&packet2);
	flow2->packet = const_cast<Packet*>(&packet2);

	// Inject two times 
        smtp->processFlow(flow1.get());

	BOOST_CHECK( info1->to != nullptr);
	BOOST_CHECK( info2->to == nullptr);

        smtp->processFlow(flow2.get());
	
	BOOST_CHECK( info1->to == info2->to);

	BOOST_CHECK( to.compare(info1->to->getName()) == 0);
}

BOOST_AUTO_TEST_SUITE_END()

