/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_dhcp.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE dhcptest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(dhcp_test_suite, StackDHCPtest)

BOOST_AUTO_TEST_CASE (test01)
{
	Packet packet;

        BOOST_CHECK(dhcp->getTotalPackets() == 0);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 0);
        BOOST_CHECK(dhcp->getTotalBytes() == 0);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);
	BOOST_CHECK(dhcp->processPacket(packet) == true);

	CounterMap c = dhcp->getCounters();
}

BOOST_AUTO_TEST_CASE (test02)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_dhcp_request);
        int length = raw_packet_ethernet_ip_udp_dhcp_request_length;
        Packet packet(pkt, length);

	inject(packet);

        // Check the results
        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 300 + 20 + 8);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(dhcp->getTotalPackets() == 1);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(dhcp->getTotalBytes() == 300);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);

	Flow *flow = dhcp->getCurrentFlow();
	BOOST_CHECK( flow != nullptr);
	SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
	BOOST_CHECK( info != nullptr);
	BOOST_CHECK( info->host_name != nullptr);

	std::string host("ctrl006");
	BOOST_CHECK(host.compare(info->host_name->getName()) == 0);
}

BOOST_AUTO_TEST_CASE (test03)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_dhcp_request_2);
        int length = raw_packet_ethernet_ip_udp_dhcp_request_length_2;
        Packet packet(pkt, length);

	inject(packet);

        // Check the results
        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 338);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(dhcp->getTotalPackets() == 1);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(dhcp->getTotalBytes() == 310);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);

	Flow *flow = dhcp->getCurrentFlow();
	BOOST_CHECK( flow != nullptr);
	SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
	BOOST_CHECK( info != nullptr);
	BOOST_CHECK( info->host_name != nullptr);

	std::string host("TurboGrafx-16");
	BOOST_CHECK(host.compare(info->host_name->getName()) == 0);
}

BOOST_AUTO_TEST_CASE (test04)
{
        auto flow = SharedPointer<Flow>(new Flow());
        unsigned char *pkt = reinterpret_cast <unsigned char*> (&raw_packet_ethernet_ip_udp_dhcp_request_2[42]);
        int length = raw_packet_ethernet_ip_udp_dhcp_request_length_2 - 42;

	// Reduce the packet size on 300 so the checks for anomalies are executed
        Packet packet(pkt, length - 300);

        flow->packet = const_cast<Packet*>(&packet);
        dhcp->processFlow(flow.get());

        // Check the results
        BOOST_CHECK(dhcp->getTotalPackets() == 1);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 0);
        BOOST_CHECK(dhcp->getTotalBytes() == 10);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);

	Flow *cflow = dhcp->getCurrentFlow();
	BOOST_CHECK( cflow != nullptr);
	SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
	BOOST_CHECK( info == nullptr);

        PacketAnomalyType pa = flow->getPacketAnomaly();
        BOOST_CHECK(pa == PacketAnomalyType::DHCP_BOGUS_HEADER);
}

BOOST_AUTO_TEST_CASE (test05)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_dhcp_ack);
        int length = raw_packet_ethernet_ip_udp_dhcp_ack_length;
        Packet packet(pkt, length);

	inject(packet);

        // Check the results
        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 300 + 20 + 8);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(dhcp->getTotalPackets() == 1);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(dhcp->getTotalBytes() == 300);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);

	Flow *flow = dhcp->getCurrentFlow();
	BOOST_CHECK( flow != nullptr);
	SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
	BOOST_CHECK( info != nullptr);
	BOOST_CHECK( info->host_name == nullptr);
}

BOOST_AUTO_TEST_CASE (test06)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_dhcp_nack);
        int length = raw_packet_ethernet_ip_udp_dhcp_nack_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(dhcp->getTotalPackets() == 1);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(dhcp->getTotalBytes() == 548);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);

	Flow *flow = dhcp->getCurrentFlow();
	BOOST_CHECK( flow != nullptr);
	SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
	BOOST_CHECK( info != nullptr);
	BOOST_CHECK( info->host_name == nullptr);
}

BOOST_AUTO_TEST_CASE (test07) // release
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_dhcp_release);
        int length = raw_packet_ethernet_ip_udp_dhcp_release_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(dhcp->getTotalPackets() == 1);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(dhcp->getTotalBytes() == 300);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);

	Flow *flow = dhcp->getCurrentFlow();
	BOOST_CHECK( flow != nullptr);
	SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
	BOOST_CHECK( info != nullptr);
	BOOST_CHECK( info->host_name == nullptr);
}

BOOST_AUTO_TEST_CASE (test08) // inform
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_dhcp_inform);
        int length = raw_packet_ethernet_ip_udp_dhcp_inform_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(dhcp->getTotalPackets() == 1);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(dhcp->getTotalBytes() == 300);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);

	Flow *flow = dhcp->getCurrentFlow();
	BOOST_CHECK( flow != nullptr);
	SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
	BOOST_CHECK( info != nullptr);
	BOOST_CHECK( info->host_name != nullptr);
	
	std::string host("TSE-MANAGEMENT");
	BOOST_CHECK(host.compare(info->host_name->getName()) == 0);
}

BOOST_AUTO_TEST_CASE (test09) // offer
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_dhcp_offer);
        int length = raw_packet_ethernet_ip_udp_dhcp_offer_length;
        Packet packet(pkt, length);

	inject(packet);

        BOOST_CHECK(dhcp->getTotalPackets() == 1);
        BOOST_CHECK(dhcp->getTotalValidatedPackets() == 1);
        BOOST_CHECK(dhcp->getTotalBytes() == 300);
        BOOST_CHECK(dhcp->getTotalMalformedPackets() == 0);

	Flow *flow = dhcp->getCurrentFlow();
	BOOST_CHECK( flow != nullptr);
	SharedPointer<DHCPInfo> info = flow->getDHCPInfo();
	BOOST_CHECK( info != nullptr);
	BOOST_CHECK( info->host_name == nullptr);
}

BOOST_AUTO_TEST_SUITE_END()
