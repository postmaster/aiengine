/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_DHCP_DHCPPROTOCOL_H_
#define SRC_PROTOCOLS_DHCP_DHCPPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_LIBLOG4CXX
#include "log4cxx/logger.h"
#endif

#include "Protocol.h"
#include <arpa/inet.h>
#include "DHCPInfo.h"
#include "StringCache.h"
//#include "CacheManager.h"
//#include <arpa/inet.h>
#include "flow/FlowManager.h"

namespace aiengine {

// ftp://ftp.isc.org/isc/dhcp/4.3.1rc1/

struct dhcp_hdr {
	uint8_t 	op;		/* packet opcode type */
	uint8_t 	htype;		/* hardware addr type */
	uint8_t 	hlen;		/* hardware addr length */
	uint8_t 	hops;		/* gateway hops */
    	uint32_t 	xid;		/* transaction ID */
    	uint16_t 	secs;		/* seconds since boot began */
    	uint16_t 	flags;		/* flags */
    	uint32_t 	ciaddr;		/* client IP address */
    	uint32_t 	yiaddr;		/* 'your' IP address */
    	uint32_t 	siaddr;		/* server IP address */
    	uint32_t 	giaddr;		/* gateway IP address */
    	u_char 		chaddr[16];	/* client hardware address */
    	u_char 		sname[64];
    	u_char 		file[128];
    	u_char 		magic[4];
    	u_char 		opt[0];
} __attribute__((packed));

enum dhcp_boot_type {
	DHCP_BOOT_REQUEST = 1,
	DHCP_BOOT_REPLY = 2
};

enum dhcp_type_code {
	DHCPDISCOVER = 1,
	DHCPOFFER,
	DHCPREQUEST,
	DHCPDECLINE,
	DHCPACK,
	DHCPNAK,
	DHCPRELEASE,
	DHCPINFORM
};

class DHCPProtocol: public Protocol {
public:
    	explicit DHCPProtocol();
    	virtual ~DHCPProtocol() {}

	static const uint16_t id = 0;	
	static constexpr int header_size = sizeof(struct dhcp_hdr);

	int getHeaderSize() const { return header_size; }

        void processFlow(Flow *flow);
        bool processPacket(Packet &packet) { return true; } 

	void statistics(std::basic_ostream<char> &out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char> &out, int level);

	void releaseCache(); 

	void setHeader(unsigned char *raw_packet) { 

		dhcp_header_ = reinterpret_cast <struct dhcp_hdr*> (raw_packet);
	}

	// Condition for say that a packet is dhcp 
	bool dhcpChecker(Packet &packet); 

	uint8_t getType() const { return dhcp_header_->op; }

        void increaseAllocatedMemory(int value);
        void decreaseAllocatedMemory(int value);

        void setFlowManager(FlowManagerPtrWeak flow_mng) { flow_mng_ = flow_mng; }

	int64_t getCurrentUseMemory() const;
        int64_t getAllocatedMemory() const;
        int64_t getTotalAllocatedMemory() const;

        void setDynamicAllocatedMemory(bool value);
        bool isDynamicAllocatedMemory() const;

	int32_t getTotalCacheMisses() const;

	CounterMap getCounters() const;

#if defined(PYTHON_BINDING)
        boost::python::dict getCache() const;
#elif defined(RUBY_BINDING)
        VALUE getCache() const;
#endif

        void setAnomalyManager(SharedPointer<AnomalyManager> amng) { anomaly_ = amng; }
        void setCacheManager(SharedPointer<CacheManager> cmng) { cache_mng_ = cmng; cache_mng_->setCache(info_cache_); }

        Flow* getCurrentFlow() const { return current_flow_; }

private:
	int32_t release_dhcp_info(DHCPInfo *info);
	int64_t compute_memory_used_by_maps() const;

	void attach_host_name(DHCPInfo *info, boost::string_ref &name);
	void handle_request(DHCPInfo *info, unsigned char *payload, int length);
	void handle_reply(DHCPInfo *info, unsigned char *payload, int length);

	struct dhcp_hdr *dhcp_header_;
        
	// Some statistics 
        int32_t total_dhcp_discover_;
        int32_t total_dhcp_offer_;
        int32_t total_dhcp_request_;
        int32_t total_dhcp_decline_;
        int32_t total_dhcp_ack_;
        int32_t total_dhcp_nak_;
        int32_t total_dhcp_release_;
        int32_t total_dhcp_inform_;

        Cache<DHCPInfo>::CachePtr info_cache_;
        Cache<StringCache>::CachePtr host_cache_;

        GenericMapType host_map_;

        FlowManagerPtrWeak flow_mng_;
        Flow *current_flow_;
#ifdef HAVE_LIBLOG4CXX
        static log4cxx::LoggerPtr logger;
#endif
        SharedPointer<AnomalyManager> anomaly_;
        SharedPointer<CacheManager> cache_mng_;
};

typedef std::shared_ptr<DHCPProtocol> DHCPProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_DHCP_DHCPPROTOCOL_H_
