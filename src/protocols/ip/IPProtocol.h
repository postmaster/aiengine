/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_IP_IPPROTOCOL_H_
#define SRC_PROTOCOLS_IP_IPPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <iostream>

namespace aiengine {

class IPProtocol: public Protocol {
public:
    	explicit IPProtocol(const std::string &name,const std::string &short_name);
    	explicit IPProtocol():IPProtocol("IPProtocol","ip") {}
    	virtual ~IPProtocol(); 

	static const uint16_t id = ETHERTYPE_IP;
	static const int header_size = 20;

	int getHeaderSize() const { return header_size; }

       	void processFlow(Flow *flow); 
	bool processPacket(Packet &packet);

	void statistics(std::basic_ostream<char> &out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char> &out, int level);

        void releaseCache() {} // No need to free cache

        void setHeader(unsigned char *raw_packet) {
        
                ip_header_ = reinterpret_cast <struct ip*> (raw_packet);
        }

	// Condition for say that a packet is IP 
	bool ipChecker(Packet &packet); 
	
	/* Fields from IP headers */
	uint8_t getTOS() const { return ip_header_->ip_tos; }
    	uint8_t getTTL() const { return ip_header_->ip_ttl; }
    	uint16_t getPacketLength() const { return ntohs(ip_header_->ip_len); }
    	uint16_t getIPHeaderLength() const { return ip_header_->ip_hl * 4; }
    	bool isIP() const { return ip_header_ ? true : false ; }
    	bool isIPver4() const { return ip_header_->ip_v == 4; }
    	bool isFragment() const { return (ip_header_->ip_off & IP_MF); }
    	uint16_t getID() const { return ntohs(ip_header_->ip_id); }
    	int getVersion() const { return ip_header_->ip_v; }
    	uint16_t getProtocol () const { return ip_header_->ip_p; }
    	uint32_t getSrcAddr() const { return ip_header_->ip_src.s_addr; }
    	uint32_t getDstAddr() const { return ip_header_->ip_dst.s_addr; }
    	const char* getSrcAddrDotNotation() const { return inet_ntoa(ip_header_->ip_src); }
    	const char* getDstAddrDotNotation() const { return inet_ntoa(ip_header_->ip_dst); }
    	uint32_t getIPPayloadLength() const { return getPacketLength() - getIPHeaderLength(); }

	int64_t getCurrentUseMemory() const { return sizeof(IPProtocol); }
	int64_t getAllocatedMemory() const { return sizeof(IPProtocol); }
	int64_t getTotalAllocatedMemory() const { return sizeof(IPProtocol); }

        void setDynamicAllocatedMemory(bool value) {}
        bool isDynamicAllocatedMemory() const { return false; }

	int32_t getTotalEvents() const { return total_events_; }

	CounterMap getCounters() const; 

	void setAnomalyManager(SharedPointer<AnomalyManager> amng) { anomaly_ = amng; }
private:
	struct ip *ip_header_;
	int32_t total_frag_packets_;
	int32_t total_events_;
	SharedPointer<AnomalyManager> anomaly_;
};

typedef std::shared_ptr<IPProtocol> IPProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_IP_IPPROTOCOL_H_
