/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_VXLAN_VXLANPROTOCOL_H_
#define SRC_PROTOCOLS_VXLAN_VXLANPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include <arpa/inet.h>

namespace aiengine {

struct vxlan_hdr {
        uint8_t		flags;   
        u_char		reserved[3];
	u_char		vni[3];
	u_char		reserv;
} __attribute__((packed));


// This class implements the Virtual Extensible Local Area Network
// that is wide spread on Cloud environments

class VxLanProtocol: public Protocol {
public:
    	explicit VxLanProtocol();
    	virtual ~VxLanProtocol() {}

	static const uint16_t id = 0;	
	static const int header_size = sizeof(struct vxlan_hdr);

	int getHeaderSize() const { return header_size; }

        void processFlow(Flow *flow);
        bool processPacket(Packet &packet) { return true; } // Nothing to process

	void statistics(std::basic_ostream<char> &out) { statistics(out, stats_level_); }
	void statistics(std::basic_ostream<char> &out, int level);

        void releaseCache() {} // No need to free cache

	void setHeader(unsigned char *raw_packet){ 

		vxlan_header_ = reinterpret_cast <struct vxlan_hdr*> (raw_packet);
	}

	// Condition for say that a packet is vxlan
	bool vxlanChecker(Packet &packet);

	uint32_t getVni() const { return ntohl(vxlan_header_->vni[2] << 24 | vxlan_header_->vni[1] << 16 | vxlan_header_->vni[0] << 8); }

	int64_t getCurrentUseMemory() const { return sizeof(VxLanProtocol); }
	int64_t getAllocatedMemory() const { return sizeof(VxLanProtocol); }
	int64_t getTotalAllocatedMemory() const { return sizeof(VxLanProtocol); }
	int64_t getAllocatedMemory(int value) const { return sizeof(VxLanProtocol); }

        void setDynamicAllocatedMemory(bool value) {}
        bool isDynamicAllocatedMemory() const { return false; }

	CounterMap getCounters() const; 

private:
	struct vxlan_hdr *vxlan_header_;
};

typedef std::shared_ptr<VxLanProtocol> VxLanProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_VXLAN_VXLANPROTOCOL_H_
