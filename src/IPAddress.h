/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_IPADDRESS_H_
#define SRC_IPADDRESS_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#if defined(__FREEBSD__) || defined(__OPENBSD__) || defined(__DARWIN__)
#include <sys/socket.h>
#define s6_addr32 __u6_addr.__u6_addr32
#else
#define s6_addr32 __in6_u.__u6_addr32
#endif

#include <iostream>
#include <memory>
#include <netinet/in.h>
#include <netinet/ip6.h>
#include <arpa/inet.h>
#include <cstring>

namespace aiengine {

class IPAddress {
public:
    	IPAddress() { reset(); }
    	virtual ~IPAddress() {}

	void reset(); 

	short getType() const { return type_; } // 4 and 6 values

	unsigned long getHash(uint16_t srcport, uint16_t protocol, uint16_t dstport); 

	uint32_t getSourceAddress() const { return ip4_src_; }
	uint32_t getDestinationAddress() const { return ip4_dst_; }
	void setSourceAddress(uint32_t address) { ip4_src_ = address; type_ = 4; }
	void setDestinationAddress(uint32_t address) { ip4_dst_ = address; type_ = 4; }
	
	void setSourceAddress6(struct in6_addr *address);
	void setDestinationAddress6(struct in6_addr *address); 
	
	struct in6_addr *getSourceAddress6() const { return const_cast<struct in6_addr*>(&ip6_src_); }
	struct in6_addr *getDestinationAddress6() const { return const_cast<struct in6_addr*>(&ip6_dst_); }

	char* getSrcAddrDotNotation() const; 
        char* getDstAddrDotNotation() const; 
 
private:
	struct in6_addr ip6_src_;
	struct in6_addr ip6_dst_;
	uint32_t ip4_src_;
	uint32_t ip4_dst_;
	short type_;
};

typedef std::shared_ptr<IPAddress> IPAddressPtr;

} // namespace aiengine

#endif  // SRC_IPADDRESS_H_
