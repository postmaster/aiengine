/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "IPSet.h"
#include <boost/asio.hpp>
#include <fstream>
#include <iomanip> // setw

namespace aiengine {

#if defined(PYTHON_BINDING) 
IPSet::IPSet(const std::string &name, boost::python::list &ips):
	IPSet(name) {

	for (int i = 0; i < len(ips); ++i ) {
		// Check if is a std::string 
		boost::python::extract<std::string> extractor(ips[i]);
		if (extractor.check()) {
			auto ip = extractor();

                        addIPAddress(ip);
		}
	}
}
#endif

void IPSet::addIPAddress(const std::string &ip) {

	boost::system::error_code err;
	boost::asio::ip::address::from_string(ip, err);
	
	if (!err) {
		map_.insert(ip);
		++total_ips_;
	}
}

bool IPSet::lookupIPAddress(const std::string &ip) {

	if (map_.find(ip) != map_.end()) {
		++total_ips_on_set_;
		return true;
	} else {
		++total_ips_not_on_set_;
		return false;
	}
}

std::ostream& operator<< (std::ostream &out, const IPSet &is) {

	out << "IPSet (" << is.getName() << ")";
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(JAVA_BINDING) || defined(LUA_BINDING)
        if (is.call.haveCallback())
                out << " Callback:" << is.call.getCallbackName();
#endif
	out << std::endl;
	//out << "\tFalse positive rate:    " << std::setw(10) << is.getFalsePositiveRate() <<std::endl;
	out << "\tTotal IP address:       " << std::setw(10) << is.total_ips_ <<std::endl;
	out << "\tTotal lookups in:       " << std::setw(10) << is.total_ips_on_set_ <<std::endl;
	out << "\tTotal lookups out:      " << std::setw(10) << is.total_ips_not_on_set_ <<std::endl;
	return out;
}

#if defined(PYTHON_BINDING)
void IPSet::show(std::basic_ostream<char> &out) const {

	out << "IPSet (" << getName() << ")";
        if (call.haveCallback())
                out << " Callback:" << call.getCallbackName();
	out << std::endl;

	for (auto &ip: map_) {
		out << "\t" << ip << "\n";
	}
	out << std::endl;
}

void IPSet::show() const {
        std::ofstream term("/dev/tty", std::ios_base::out);
        show(term);
        term.close();
}

#endif

} // namespace aiengine
