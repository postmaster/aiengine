In this section we are going to explore and compare the different performance values such as CPU and memory comsumption
with other engines such as tshark, snort and suricata.

The pcap file use is from (http://www.unb.ca/cic/research/datasets/index.html) is aproximately 17GB size with the mayority of traffic HTTP.

On the other hand, the tools used for evaluate the performance and comsumption with be perf and massif.

Snort version used 2.9.9.0
Tshark version 2.0.2
AIEngine version 1.7.1
Suricata version 3.2.1

The machine is a 8 CPUS Intel(R) Core(TM) i7-6820HQ CPU @ 2.70GHz with 16 GB memory. 


Processing traffic
~~~~~~~~~~~~~~~~~~

In this section we explore how fast are the engines just processing the traffic without any rules or any logic on them.

Snort
*****

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

      64269.015098      task-clock (msec)         #    0.981 CPUs utilized          
             1,760      context-switches          #    0.027 K/sec                  
                36      cpu-migrations            #    0.001 K/sec                  
            44,841      page-faults               #    0.698 K/sec                  
   204,394,163,771      cycles                    #    3.180 GHz                    
   375,256,677,520      instructions              #    1.84  insns per cycle        
    98,031,161,725      branches                  # 1525.325 M/sec                  
       565,404,035      branch-misses             #    0.58% of all branches        

      65.487290231 seconds time elapsed


Tshark
******

.. code:: bash

   Performance counter stats for 'tshark -q -z conv,tcp -r /pcaps/iscx/testbed-17jun.pcap':

     112070.498904      task-clock (msec)         #    0.909 CPUs utilized          
            11,390      context-switches          #    0.102 K/sec                  
               261      cpu-migrations            #    0.002 K/sec                  
         2,172,942      page-faults               #    0.019 M/sec                  
   310,196,020,123      cycles                    #    2.768 GHz                    
   449,687,949,322      instructions              #    1.45  insns per cycle        
    99,620,662,743      branches                  #  888.911 M/sec                  
       729,598,416      branch-misses             #    0.73% of all branches        

     123.265736897 seconds time elapsed


AIengine
********

.. code:: bash

    Performance counter stats for './aiengine -i /pcaps/iscx/testbed-17jun.pcap -o':

      18039.084521      task-clock (msec)         #    0.704 CPUs utilized          
            91,186      context-switches          #    0.005 M/sec                  
               476      cpu-migrations            #    0.026 K/sec                  
            17,430      page-faults               #    0.966 K/sec                  
    50,049,479,531      cycles                    #    2.775 GHz                    
    67,835,601,309      instructions              #    1.36  insns per cycle        
    14,522,520,700      branches                  #  805.059 M/sec                  
       166,645,083      branch-misses             #    1.15% of all branches        

      25.610078128 seconds time elapsed


Suricata
********

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     100446.349460      task-clock (msec)         #    3.963 CPUs utilized          
         2,264,381      context-switches          #    0.023 M/sec                  
           220,905      cpu-migrations            #    0.002 M/sec                  
           108,722      page-faults               #    0.001 M/sec                  
   274,824,170,581      cycles                    #    2.736 GHz                    
   249,152,605,118      instructions              #    0.91  insns per cycle        
    56,052,176,697      branches                  #  558.031 M/sec                  
       538,776,158      branch-misses             #    0.96% of all branches        

      25.345742192 seconds time elapsed

Tests with rules
~~~~~~~~~~~~~~~~

On this section we evalute simple rules in order to compare the different systems.

The rule that we are going to use is quite simple, it consists on find the string "cmd.exe" on the payload of all the TCP traffic.

Snort
*****

Rule: alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1)

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

     271091.019789      task-clock (msec)         #    0.994 CPUs utilized          
             3,213      context-switches          #    0.012 K/sec                  
                80      cpu-migrations            #    0.000 K/sec                  
            65,124      page-faults               #    0.240 K/sec                  
   731,608,435,272      cycles                    #    2.699 GHz                    
 1,033,203,748,622      instructions              #    1.41  insns per cycle        
   193,558,431,134      branches                  #  713.998 M/sec                  
       655,588,320      branch-misses             #    0.34% of all branches        

     272.704320602 seconds time elapsed

AIEngine
********

Rule: "cmd.exe"

.. code:: bash

   Performance counter stats for './aiengine -i /pcaps/iscx/testbed-17jun.pcap -R -r cmd.exe -m -c tcp':

      25815.755873      task-clock (msec)         #    0.945 CPUs utilized          
            38,008      context-switches          #    0.001 M/sec                  
               109      cpu-migrations            #    0.004 K/sec                  
             2,587      page-faults               #    0.100 K/sec                  
    79,739,426,930      cycles                    #    3.089 GHz                    
   170,908,065,937      instructions              #    2.14  insns per cycle        
    48,678,620,025      branches                  # 1885.617 M/sec                  
       438,618,038      branch-misses             #    0.90% of all branches        

      27.323182412 seconds time elapsed


Suricata
********

Rule: alert tcp any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     140614.443416      task-clock (msec)         #    5.048 CPUs utilized          
         1,325,849      context-switches          #    0.009 M/sec                  
            79,489      cpu-migrations            #    0.565 K/sec                  
           292,515      page-faults               #    0.002 M/sec                  
   387,832,364,297      cycles                    #    2.758 GHz                    
   427,644,070,747      instructions              #    1.10  insns per cycle        
    80,760,364,199      branches                  #  574.339 M/sec                  
       570,378,374      branch-misses             #    0.71% of all branches        

      27.855318250 seconds time elapsed

Snort
*****

Rule: alert tcp any any -> any 80 (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash

   Performance counter stats for './snort -r /pcaps/iscx/testbed-17jun.pcap -c ./snort.conf':

      70456.213488      task-clock (msec)         #    0.984 CPUs utilized          
             5,901      context-switches          #    0.084 K/sec                  
                63      cpu-migrations            #    0.001 K/sec                  
            79,927      page-faults               #    0.001 M/sec                  
   214,846,354,228      cycles                    #    3.049 GHz                    
   385,107,871,838      instructions              #    1.79  insns per cycle        
   100,011,250,526      branches                  # 1419.481 M/sec                  
       579,460,528      branch-misses             #    0.58% of all branches        

      71.582493144 seconds time elapsed


AIEngine
********

.. code:: python

  def http_callback(flow):
    print("rule on HTTP %s" % str(flow))

  if __name__ == '__main__':

    st = StackLan()

    http = DomainNameManager() 
    rm = RegexManager()
    r = Regex("my cmd.exe", "cmd.exe")
    r.callback = anomaly_callback

    d1 = DomainName("Generic net",".net")
    d2 = DomainName("Generic com",".com")
    d3 = DomainName("Generic org",".org")
 
    http.add_domain_name(d1) 
    http.add_domain_name(d2) 
    http.add_domain_name(d3) 

    d1.regex_manager = rm
    d2.regex_manager = rm
    d3.regex_manager = rm

    rm.add_regex(r)

    st.set_domain_name_manager(http,"HTTPProtocol")

    st.set_dynamic_allocated_memory(True)
    
    with pyaiengine.PacketDispatcher("/pcaps/iscx/testbed-17jun.pcap") as pd:
            pd.stack = st
            pd.run()

    sys.exit(0)

.. code:: bash

   Performance counter stats for 'python http_cmd.exe.py':

      23911.066270      task-clock (msec)         #    0.846 CPUs utilized          
            62,787      context-switches          #    0.003 M/sec                  
               455      cpu-migrations            #    0.019 K/sec                  
            11,419      page-faults               #    0.478 K/sec                  
    65,906,204,338      cycles                    #    2.756 GHz                    
   112,010,669,504      instructions              #    1.70  insns per cycle        
    28,798,288,663      branches                  # 1204.392 M/sec                  
       271,377,464      branch-misses             #    0.94% of all branches        

      28.273167310 seconds time elapsed

Suricata
********

Rule: alert http any any -> any any (content:"cmd.exe"; msg:"Traffic with cmd.exe on it"; sid:1; rev:1;)

.. code:: bash

   Performance counter stats for './suricata -c suricata.yaml -r /pcaps/iscx/testbed-17jun.pcap':

     140314.604419      task-clock (msec)         #    5.007 CPUs utilized          
         1,326,047      context-switches          #    0.009 M/sec                  
            81,882      cpu-migrations            #    0.584 K/sec                  
           287,767      page-faults               #    0.002 M/sec                  
   385,297,597,444      cycles                    #    2.746 GHz                    
   427,295,175,085      instructions              #    1.11  insns per cycle        
    80,682,776,679      branches                  #  575.013 M/sec                  
       570,289,598      branch-misses             #    0.71% of all branches        

      28.023789653 seconds time elapsed

