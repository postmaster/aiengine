#ifndef _dhcp_packets_H_
#define _dhcp_packets_H_

// A Mini database of different dhcp packets

// ethernet, ip, udp, dhcp offer
static char *raw_packet_ethernet_ip_udp_dhcp_offer =       
        "\x00\x0c\x29\xc0\xb9\xa8\x00\x50\x56\xfa\xa0\x55\x08\x00\x45\x10"
        "\x01\x48\x00\x00\x00\x00\x10\x11\xd6\xbd\xc0\xa8\x28\xfe\xc0\xa8"
        "\x28\x89\x00\x43\x00\x44\x01\x34\xe3\xaa\x02\x01\x06\x00\x66\xc8"
        "\x02\x4a\x00\x00\x00\x00\x00\x00\x00\x00\xc0\xa8\x28\x89\xc0\xa8"
        "\x28\xfe\x00\x00\x00\x00\x00\x0c\x29\xc0\xb9\xa8\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
        "\x00\x00\x00\x00\x00\x00\x63\x82\x53\x63\x35\x01\x02\x36\x04\xc0"
        "\xa8\x28\xfe\x33\x04\x00\x00\x07\x08\x01\x04\xff\xff\xff\x00\x1c"
        "\x04\xc0\xa8\x28\xff\x03\x04\xc0\xa8\x28\x02\x0f\x0b\x6c\x6f\x63"
        "\x61\x6c\x64\x6f\x6d\x61\x69\x6e\x06\x04\xc0\xa8\x28\x02\x2c\x04"
        "\xc0\xa8\x28\x02\xff\x00";
static int raw_packet_ethernet_ip_udp_dhcp_offer_length= 342; 

// ip.src 10.64.91.181
// ip.dst 10.64.91.1
// dhcp request
// client ip 10.64.91.181
// client mac address 08:00:27:ee:7e:c3
static char *raw_packet_ethernet_ip_udp_dhcp_request =
	"\x08\x00\x27\xf8\xd4\x96\x08\x00\x27\xee\x7e\xc3\x08\x00\x45\x00"
	"\x01\x48\x18\x19\x00\x00\x80\x11\x56\x56\x0a\x40\x5b\xb5\x0a\x40"
	"\x5b\x01\x00\x44\x00\x43\x01\x34\xae\x40\x01\x01\x06\x00\x1d\xf2"
	"\x12\xf4\x00\x00\x00\x00\x0a\x40\x5b\xb5\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x08\x00\x27\xee\x7e\xc3\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x63\x82\x53\x63\x35\x01\x03\x3d\x07\x01"
	"\x08\x00\x27\xee\x7e\xc3\x0c\x07\x63\x74\x72\x6c\x30\x30\x36\x51"
	"\x0b\x00\x00\x00\x63\x74\x72\x6c\x30\x30\x36\x2e\x3c\x08\x4d\x53"
	"\x46\x54\x20\x35\x2e\x30\x37\x0b\x01\x0f\x03\x06\x2c\x2e\x2f\x1f"
	"\x21\xf9\x2b\xff\x00\x00";

static int raw_packet_ethernet_ip_udp_dhcp_request_length = 342;

// Host name:TurboGrafx-16
// client fully domain: TurboGrafx-16
static char *raw_packet_ethernet_ip_udp_dhcp_request_2 =
	"\x00\x26\x82\xfb\x93\x08\xc0\xcb\x38\x42\xdb\x58\x08\x00\x45\x00"
	"\x01\x52\x19\x70\x00\x00\x80\x11\x9a\xc5\xc0\xa8\x02\x14\xc0\xa8"
	"\x02\x01\x00\x44\x00\x43\x01\x3e\x38\xc7\x01\x01\x06\x00\xe7\x28"
	"\xc4\x63\x00\x00\x00\x00\xc0\xa8\x02\x14\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\xc0\xcb\x38\x42\xdb\x58\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x63\x82\x53\x63\x35\x01\x03\x3d\x07\x01"
	"\xc0\xcb\x38\x42\xdb\x58\x0c\x0d\x54\x75\x72\x62\x6f\x47\x72\x61"
	"\x66\x78\x2d\x31\x36\x51\x10\x00\x00\x00\x54\x75\x72\x62\x6f\x47"
	"\x72\x61\x66\x78\x2d\x31\x36\x3c\x08\x4d\x53\x46\x54\x20\x35\x2e"
	"\x30\x37\x0c\x01\x0f\x03\x06\x2c\x2e\x2f\x1f\x21\x79\xf9\x2b\xff";

static int raw_packet_ethernet_ip_udp_dhcp_request_length_2 = 352;

static char *raw_packet_ethernet_ip_udp_dhcp_ack =
	"\x00\x0b\x82\x01\xfc\x42\x00\x08\x74\xad\xf1\x9b\x08\x00\x45\x00"
	"\x01\x48\x04\x46\x00\x00\x80\x11\x00\x00\xc0\xa8\x00\x01\xc0\xa8"
	"\x00\x0a\x00\x43\x00\x44\x01\x34\xdf\xdb\x02\x01\x06\x00\x00\x00"
	"\x3d\x1e\x00\x00\x00\x00\x00\x00\x00\x00\xc0\xa8\x00\x0a\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x0b\x82\x01\xfc\x42\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x63\x82\x53\x63\x35\x01\x05\x3a\x04\x00"
	"\x00\x07\x08\x3b\x04\x00\x00\x0c\x4e\x33\x04\x00\x00\x0e\x10\x36"
	"\x04\xc0\xa8\x00\x01\x01\x04\xff\xff\xff\x00\xff\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00";

static int raw_packet_ethernet_ip_udp_dhcp_ack_length = 342;

static char *raw_packet_ethernet_ip_udp_dhcp_nack =
	"\xff\xff\xff\xff\xff\xff\x00\x0f\x66\xc8\xeb\xc9\x08\x00\x45\x00"
	"\x02\x40\x00\x00\x00\x00\x40\x11\xb7\x04\xc0\xa8\x01\x01\xff\xff"
	"\xff\xff\x00\x43\x00\x44\x02\x2c\x58\x2f\x02\x01\x06\x00\xe0\x1c"
	"\xc7\x18\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x28\xcf\xda\xdc\x8d\x76\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x63\x82\x53\x63\x35\x01\x06\x36\x04\xc0"
	"\xa8\x01\x01\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";

static int raw_packet_ethernet_ip_udp_dhcp_nack_length = 590;

static char *raw_packet_ethernet_ip_udp_dhcp_release =
	"\x00\x0c\x29\x94\xad\x2d\x00\x0c\x29\xc4\x43\xd3\x08\x00\x45\x00"
	"\x01\x48\x01\xeb\x00\x00\x80\x11\x1a\x29\x0a\x49\x04\x02\x0a\x49"
	"\x04\xfe\x00\x44\x00\x43\x01\x34\x50\x73\x01\x01\x06\x00\xb4\x43"
	"\x9a\xb8\x0b\x00\x00\x00\x0a\x49\x04\x02\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x0c\x29\xc4\x43\xd3\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x63\x82\x53\x63\x35\x01\x07\x36\x04\x0a"
	"\x49\x04\xfe\x3d\x07\x01\x00\x0c\x29\xc4\x43\xd3\xff\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00";

static int raw_packet_ethernet_ip_udp_dhcp_release_length = 342;

static char *raw_packet_ethernet_ip_udp_dhcp_inform =
	"\xff\xff\xff\xff\xff\xff\x00\x0c\x29\xc4\x43\xd3\x08\x00\x45\x00"
	"\x01\x48\x02\x0c\x00\x00\x80\x11\x29\x4f\x0a\x49\x04\x02\xff\xff"
	"\xff\xff\x00\x44\x00\x43\x01\x34\x85\x57\x01\x01\x06\x00\xa9\x13"
	"\x64\x77\x00\x00\x80\x00\x0a\x49\x04\x02\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x0c\x29\xc4\x43\xd3\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x63\x82\x53\x63\x35\x01\x08\x3d\x07\x01"
	"\x00\x0c\x29\xc4\x43\xd3\x0c\x0e\x54\x53\x45\x2d\x4d\x41\x4e\x41"
	"\x47\x45\x4d\x45\x4e\x54\x3c\x08\x4d\x53\x46\x54\x20\x35\x2e\x30"
	"\x37\x0d\x01\x0f\x03\x06\x2c\x2e\x2f\x1f\x21\x79\xf9\x2b\xfc\xff"
	"\x00\x00\x00\x00\x00\x00";

static int raw_packet_ethernet_ip_udp_dhcp_inform_length = 342;

#endif
