#ifndef _pppoe_test_packets_H_
#define _pppoe_test_packets_H_


static char *raw_ethernet_pppoe_ipcp =
	"\xca\x01\x0e\x88\x00\x06\xcc\x05\x0e\x88\x00\x00\x88\x64\x11\x00"
	"\x00\x11\x00\x0c\xc0\x21\x01\x01\x00\x0a\x05\x06\x05\xfc\xd4\x59"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"
	"\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";

static int raw_ethernet_pppoe_ipcp_length = 60;

static char *raw_ethernet_pppoe_ipv6_icmpv6_ra =
	"\xcc\x05\x0e\x88\x00\x00\xca\x01\x0e\x88\x00\x06\x88\x64\x11\x00"
	"\x00\x11\x00\x42\x00\x57\x6e\x00\x00\x00\x00\x18\x3a\xff\xfe\x80"
	"\x00\x00\x00\x00\x00\x00\xc8\x01\x0e\xff\xfe\x88\x00\x08\xfe\x80"
	"\x00\x00\x00\x00\x00\x00\xce\x05\x0e\xff\xfe\x88\x00\x00\x86\x00"
	"\x79\xae\x40\x00\x07\x08\x00\x00\x00\x00\x00\x00\x00\x00\x05\x01"
	"\x00\x00\x00\x00\x05\xd4";

static int raw_ethernet_pppoe_ipv6_icmpv6_ra_length = 86;

static char *raw_ethernet_pppoe_ipv4_tcp_syn = 
	"\x00\x90\x1a\xa0\x3d\xa4\x00\x11\xf5\x13\xd7\xa3\x88\x64\x11\x00"
	"\x06\x9d\x00\x32\x00\x21\x45\x00\x00\x30\x01\x37\x40\x00\x80\x06"
	"\x58\xa7\xac\xca\xf6\x39\x40\x0c\xbd\xd9\x04\x27\x01\xbb\xe9\x42"
	"\x87\x55\x00\x00\x00\x00\x70\x02\xff\xff\x6b\xed\x00\x00\x02\x04"
	"\x05\x82\x01\x01\x04\x02";

static int raw_ethernet_pppoe_ipv4_tcp_syn_length = 70;

#endif
